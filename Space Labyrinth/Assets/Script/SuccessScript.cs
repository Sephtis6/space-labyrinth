﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SuccessScript : MonoBehaviour
{
    public bool trickExit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (trickExit == false)
        {
            SceneManager.LoadScene(3);
        }
        if (trickExit == true)
        {
            SceneManager.LoadScene(4);
        }
    }
}
