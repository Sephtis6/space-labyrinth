﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{

    [HideInInspector] public HumanController CC;
    [HideInInspector] public Transform tf;

    //integers and floats named
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Animator anim;

    public float moveSpeed;

    public bool XorY;


    public void Start()
    {
        CC = GameObject.Find("CharacterController").GetComponent<HumanController>();
        CC.pawn = GetComponent<Pawn>();
        tf = GetComponent<Transform>();
        CC.setPawn();

        // Get my components
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    public void Update()
    {

    }

    public void Move(Vector2 moveVector)
    {
        // Change X velocity based on speed and moveVector
        if (XorY == true)
        {
            rb.velocity = new Vector2(moveVector.x * moveSpeed, rb.velocity.y);
        }
        // Change Y velocity based on speed and moveVector
        if (XorY == false)
        {
            rb.velocity = new Vector2(rb.velocity.x, moveVector.y * moveSpeed);
        }
    }
}