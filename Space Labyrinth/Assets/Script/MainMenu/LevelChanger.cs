﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    private void Update()
    {

    }

    //allows to set what level to go to on button press
    public void LoadByIndex(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }



    //quit the game apllication
    public void Quit()
    {
        //if untiy editor
        //UnityEditor.EditorApplication.isPlaying = false;
        //else
        Application.Quit();
    }
}
