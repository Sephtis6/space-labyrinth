﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [HideInInspector] public GameManager gm;

    public Text volumeAudioText;
    public Text sfxAudioText;

    public Slider volumeSlider;
    public Slider sfxSlider;

    // Use this for initialization
    void Start ()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        
	}


    //sets the music volume to a slider and prints the number as text
    public void VolumeController()
    {
        gm.volumeAudio.volume = volumeSlider.value;
        setVolumeAudioTextText();
    }
    //sets the sfx volume to a slider and prints the number as text
    public void SFXController()
    {
        gm.sfxAudio.volume = sfxSlider.value;
        setSFXAudioTextText();
    }

    //sets the music volume number as text
    void setVolumeAudioTextText()
    {
        volumeAudioText.text = "Music Volume: " + volumeSlider.value.ToString();
    }
    //sets the sfx volume number as text
    void setSFXAudioTextText()
    {
        sfxAudioText.text = "SFX Volume: " + sfxSlider.value.ToString();
    }

    public void buttonPressSound()
    {
       gm.sfxAudio.PlayOneShot(gm.buttonPress, gm.sfxAudio.volume);
    }
}
