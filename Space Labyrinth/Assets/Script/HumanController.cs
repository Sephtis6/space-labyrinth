﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HumanController : Controller {

    public Transform tf;
    public float speed;
    public float turnspeed;
    public FollowPlayerCamera FPC;

    public float lives = 3;
    public Text LifeText;

    [HideInInspector] public GameManager gm;
    public GameObject playerPrefab;
    public GameObject Exit;
    public float timeToReveal;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        setLifeText();
        setPawn();
        gm.volumeAudio.clip = gm.audioClips[1];
        gm.volumeAudio.loop = true;
        gm.volumeAudio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movementVector = Vector2.zero;
        //move straight
        if (Input.GetKey(KeyCode.W))
        {
            movementVector.y = 10;
            pawn.XorY = false;
        }
        //rotate left
        if (Input.GetKey(KeyCode.A))
        {
            movementVector.x = -10;
            pawn.XorY = true;
        }
        //rotate right
        if (Input.GetKey(KeyCode.D))
        {
            movementVector.x = 10;
            pawn.XorY = true;
        }
        //move backwards
        if (Input.GetKey(KeyCode.S))
        {
            movementVector.y = -10;
            pawn.XorY = false;
        }
        pawn.Move(movementVector);
        if (lives == 1)
        {
            timeToReveal -= Time.deltaTime;
            if (timeToReveal <= 0.0f)
            {
                Exit.SetActive(true);
            }
        }
        if (lives <= 0)
        {
            SceneManager.LoadScene(4);
        }
    }

    public void setLifeText()
    {
        LifeText.text = "Lives Left: " + lives.ToString();
    }

    public void InstantiatePlayer()
    {
        Instantiate(playerPrefab, transform.position, Quaternion.identity);
    }

    public void setPawn()
    {
        gm.playerPawn = pawn;
    }
}
